package me.relevante.backend.queue;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableRabbit
@EnableAutoConfiguration
@ComponentScan("me.relevante")
public class EmailsConsumerApp {
	
	public static void main(String[] args) {
		SpringApplication.run(EmailsConsumerApp.class, args);
	}
}