package me.relevante.backend.queue.persistence;

import com.mongodb.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MongoDBConnection implements MongoConnection {

	private static final Logger logger = LoggerFactory.getLogger(MongoDBConnection.class);

    private MongoClient mongoClient;
    private String databaseName;

    @Autowired
    public MongoDBConnection(@Value("${mongodb.host}") String host,
                             @Value("${mongodb.port}") int port,
                             @Value("${mongodb.databaseName}") String databaseName) {
        this.mongoClient = createMongoClient(host, port);
        this.databaseName = databaseName;
    }

    @Override
    public MongoClient getMongoClient() {
        return mongoClient;
    }

    @Override
    public String getDatabaseName() {
        return databaseName;
    }

    private MongoClient createMongoClient(String host,
                                          int port) {
		MongoClient mongo = null;
		try {
			mongo = new MongoClient(host, port);
			logger.debug("New Mongo created with [" + host + "] and ["
					+ port + "]");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return mongo;
	}
}
