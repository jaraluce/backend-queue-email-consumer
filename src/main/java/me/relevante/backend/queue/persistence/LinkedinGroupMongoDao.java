package me.relevante.backend.queue.persistence;

import me.relevante.linkedin.model.LinkedinGroup;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LinkedinGroupMongoDao extends BasicDAO<LinkedinGroup, ObjectId> {

	private UpdateOperations<LinkedinGroup> ops;

    @Autowired
	public LinkedinGroupMongoDao(MongoConnection mongoConnection) {
		super(mongoConnection.getMongoClient(), new Morphia().map(LinkedinGroup.class), mongoConnection.getDatabaseName());
	}

	public void updateGroupKeywords(LinkedinGroup group) {
		Query<LinkedinGroup> updateQuery =
				getDs().createQuery(LinkedinGroup.class).field("groupId").equal(group.getGroupId());

		ops = getDs().createUpdateOperations(LinkedinGroup.class).set("keywords", group.getKeywords());
		getDs().update(updateQuery, ops);
		ops = getDs().createUpdateOperations(LinkedinGroup.class).set("stems", group.getStems());
		getDs().update(updateQuery, ops);
	}

    @Override
    public Key<LinkedinGroup> save(LinkedinGroup group){
        LinkedinGroup persistedLinkedinGroup = findByGroupId(group.getGroupId());
        if (persistedLinkedinGroup != null) {
            group.setId(persistedLinkedinGroup.getId());
        }
        return super.save(group);

    }

	public LinkedinGroup findByGroupId(String groupId) {
		LinkedinGroup linkedinGroup = getDs()
				.find(LinkedinGroup.class, "groupId", groupId)
				.limit(1)
				.get();
		return linkedinGroup;
	}

    public List<LinkedinGroup> findAll() {
		Query<LinkedinGroup> q = getDs().find(LinkedinGroup.class);
		return q.asList();
	}

    public List<LinkedinGroup> getListByGroupIds(List<String> groupIds) {

        List<LinkedinGroup> sortedUserIds = getDs().createQuery(LinkedinGroup.class).field("groupId").in(groupIds).asList();

        return sortedUserIds;
    }


}
