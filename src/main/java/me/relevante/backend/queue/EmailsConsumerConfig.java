package me.relevante.backend.queue;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class EmailsConsumerConfig {
	
	@Value("${spring.email.host}")
	private String host;
	@Value("${spring.email.port}")
	private int port;
	@Value("${spring.email.username}")
	private String username;
	@Value("${spring.email.password}")
	private String password;
	@Value("${spring.email.useAuth}")
	private Boolean userAuth;
	@Value("${spring.email.useTtls}")
	private Boolean useTtls;

	public @Bean JavaMailSenderImpl emailSender() {
		JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
		javaMailSenderImpl.setHost(host);
		javaMailSenderImpl.setPort(port);
		javaMailSenderImpl.setUsername(username);
		javaMailSenderImpl.setPassword(password);
		Properties javaMailProperties = new Properties();
		javaMailProperties.setProperty("mail.smtp.auth", String.valueOf(userAuth));
		javaMailProperties.setProperty("mail.smtp.starttls.enable", String.valueOf(useTtls));
		javaMailSenderImpl.setJavaMailProperties(javaMailProperties);
		return javaMailSenderImpl;
	}
}
