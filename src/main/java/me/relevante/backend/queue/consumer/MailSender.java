package me.relevante.backend.queue.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import me.relevante.backend.queue.errorHandler.SendEmailRequest;

@Component
public class MailSender {

	@Autowired
	private JavaMailSenderImpl javaMailSenderImpl;
	
	public void sendMail(String from, String to, String subject, String msg) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(from);
		message.setTo(to.split(","));
		message.setSubject(subject);
		message.setText(msg);
		javaMailSenderImpl.send(message);	
	}

	public void sendMail(SendEmailRequest sendEmailRequest) {
		this.sendMail(sendEmailRequest.getFrom(), sendEmailRequest.getTo(), sendEmailRequest.getSubject(), sendEmailRequest.getContent());
	}
}
