package me.relevante.backend.queue.consumer;

import me.relevante.backend.queue.QueueNames;
import me.relevante.backend.queue.persistence.LinkedinUserRepo;
import me.relevante.backend.queue.persistence.RelatedUserRepo;
import me.relevante.backend.queue.requests.ProcessContactsRequest;
import me.relevante.linkedin.api.LinkedinApi;
import me.relevante.linkedin.api.LinkedinApiForbiddenException;
import me.relevante.linkedin.model.LinkedinUser;
import me.relevante.model.Network;
import me.relevante.model.RelatedUser;
import me.relevante.model.RelatedUserId;
import me.relevante.model.oauth.OAuthKeyPair;
import me.relevante.model.oauth.OAuthTokenPair;
import me.relevante.nlp.Classifier;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;

public class LinkedInContactsConsumer extends BaseConsumer {
	
	private Logger logger = LoggerFactory.getLogger(LinkedInGroupsConsumer.class);

    @Autowired
    private LinkedinUserRepo linkedinUserRepo;
    @Autowired
    private RelatedUserRepo relatedUserRepo;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private Classifier classifier;
    @Autowired
    private OAuthKeyPair oAuthConsumerKeyPair;

	@RabbitListener(queues = QueueNames.LINKEDIN_CONTACTS)
	public void processContactsLinkedin(byte[] data) throws InterruptedException, IOException {

		ProcessContactsRequest processContactsRequest = objectMapper.readValue(data, ProcessContactsRequest.class);
        OAuthTokenPair oAuthTokenPair = new OAuthTokenPair(processContactsRequest.getToken(), processContactsRequest.getSecret());
        LinkedinApi linkedinAPI = new LinkedinApi(oAuthConsumerKeyPair, oAuthTokenPair);

        try {
            List<LinkedinUser> connections = linkedinAPI.getConnections();
            for (LinkedinUser connection : connections) {
                processConnection(connection, processContactsRequest.getIdRelevante());
            }
        }
        catch (LinkedinApiForbiddenException e) {
            throw new InterruptedException(e.getMessage());
        }
	}

    private void processConnection(LinkedinUser connection,
                                   int relevanteId) throws IOException {

        LinkedinUser alreadyPersistedUser = linkedinUserRepo.findOne(connection.getLinkedinId());
        if (alreadyPersistedUser != null) {
            connection.updateDataFromUser(alreadyPersistedUser);
            if (!StringUtils.isEmpty(alreadyPersistedUser.getKeywords())) {
                logger.info("Found...previous keywords were: " + alreadyPersistedUser.getKeywords());
            }
        }
        classifier.assignKeywords(connection);
        linkedinUserRepo.save(connection);

        if (!relatedUserRepo.exists(new RelatedUserId(relevanteId, connection.getLinkedinId()))) {
            RelatedUser relatedUser = new RelatedUser(relevanteId, connection.getLinkedinId(), Network.LINKEDIN, true);
            relatedUserRepo.save(relatedUser);
        }

        rabbitTemplate.send(QueueNames.LINKEDIN_CONTACT_KEYWORDS, MessageBuilder.withBody(connection.getLinkedinId().getBytes()).build());
    }

}