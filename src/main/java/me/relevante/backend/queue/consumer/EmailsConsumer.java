package me.relevante.backend.queue.consumer;

import me.relevante.backend.queue.QueueNames;
import me.relevante.backend.queue.errorHandler.SendEmailRequest;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class EmailsConsumer extends BaseConsumer {

    @Autowired
    private MailSender mailSender;

    @RabbitListener(queues = QueueNames.EMAILS)
    public void sendEmail(byte[] data) throws IOException {
    	SendEmailRequest sendEmailRequest = objectMapper.readValue(data, SendEmailRequest.class);
    	mailSender.sendMail(sendEmailRequest);
    }
}