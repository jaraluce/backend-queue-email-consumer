package me.relevante.backend.queue.consumer;

import me.relevante.backend.queue.QueueNames;
import me.relevante.backend.queue.persistence.LinkedinGroupMongoDao;
import me.relevante.backend.queue.persistence.LinkedinPostMongoDao;
import me.relevante.backend.queue.persistence.LinkedinUserRepo;
import me.relevante.backend.queue.requests.ProcessContactsRequest;
import me.relevante.linkedin.api.LinkedinApi;
import me.relevante.linkedin.api.LinkedinApiForbiddenException;
import me.relevante.linkedin.model.LinkedinGroup;
import me.relevante.linkedin.model.LinkedinPost;
import me.relevante.linkedin.model.LinkedinUser;
import me.relevante.model.oauth.OAuthKeyPair;
import me.relevante.model.oauth.OAuthTokenPair;
import me.relevante.nlp.Classifier;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LinkedInGroupsConsumer extends BaseConsumer {

    private static int MAX_SHARES_PER_GROUP = 100;

    @Autowired
    private LinkedinUserRepo linkedinUserRepo;
    @Autowired
    private LinkedinGroupMongoDao linkedinGroupDao;
    @Autowired
    private LinkedinPostMongoDao linkedinPostDao;
    @Autowired
    private Classifier classifier;
    @Autowired
    private OAuthKeyPair oAuthConsumerKeyPair;

    @RabbitListener(queues = QueueNames.LINKEDIN_GROUPS)
    public void processGroupsLinkedin(byte[] data) throws InterruptedException, IOException {

        ProcessContactsRequest processContactsRequest = objectMapper.readValue(data, ProcessContactsRequest.class);
        OAuthTokenPair oAuthTokenPair = new OAuthTokenPair(processContactsRequest.getToken(), processContactsRequest.getSecret());
        LinkedinApi linkedinApi = new LinkedinApi(oAuthConsumerKeyPair, oAuthTokenPair);

        List<LinkedinGroup> linkedinGroups = linkedinApi.getUserGroups();
        for (LinkedinGroup linkedinGroup : linkedinGroups) {
            try {
                processLinkedinGroup(linkedinGroup, linkedinApi);
            }
            catch (LinkedinApiForbiddenException e) {
                throw new InterruptedException(e.getMessage());
            }
        }

    }

    private void processLinkedinGroup(LinkedinGroup linkedinGroup,
                                      LinkedinApi linkedinApi) throws LinkedinApiForbiddenException, IOException {

        classifier.assignKeywords(linkedinGroup);
        linkedinGroupDao.save(linkedinGroup);

        List<LinkedinPost> linkedinGroupShares = linkedinApi.getGroupShares(linkedinGroup.getGroupId(), MAX_SHARES_PER_GROUP);

        List<LinkedinUser> linkedinUsers = getAllUsersFromShares(linkedinGroupShares);
        for (LinkedinUser linkedinUser : linkedinUsers) {
            populateUser(linkedinUser, linkedinApi);
        }

        for (LinkedinPost linkedinGroupShare : linkedinGroupShares) {
            LinkedinPost persistedLinkedinPost = linkedinPostDao.findByPostId(linkedinGroupShare.getPostId());
            if (persistedLinkedinPost == null || persistedLinkedinPost.getKeywords() == null || persistedLinkedinPost.getKeywords().isEmpty()) {
                if (linkedinGroupShare.getAuthor() != null && linkedinGroupShare.getAuthor().getUrl() != null) {
                    classifier.assignKeywords(linkedinGroupShare);
                    linkedinPostDao.save(linkedinGroupShare);
                }
            }
        }

        for (LinkedinUser linkedinUser : linkedinUsers) {
            if (linkedinUser.getUrl() != null) {
                classifier.assignKeywords(linkedinUser);
                linkedinUserRepo.save(linkedinUser);
            }
        }
    }

    private List<LinkedinUser> getAllUsersFromShares(List<LinkedinPost> linkedinGroupShares) {

        // Create a map containing unique users as much updated as possible
        Map<String, LinkedinUser> usersMap = new HashMap<>();
        for (LinkedinPost linkedinGroupShare : linkedinGroupShares) {
            LinkedinUser userInMap = usersMap.get(linkedinGroupShare.getUserId());
            if (userInMap == null) {
                usersMap.put(linkedinGroupShare.getAuthor().getLinkedinId(), linkedinGroupShare.getAuthor());
            }
            else {
                userInMap.updateDataFromUser(linkedinGroupShare.getAuthor());
            }
        }

        // Put the obtained users into a list to be returned
        List<LinkedinUser> linkedinUsers = new ArrayList<>(usersMap.values());

        return linkedinUsers;
    }

    private void populateUser(LinkedinUser user,
                              LinkedinApi linkedinApi) {
        LinkedinUser persistedUser = linkedinUserRepo.findOne(user.getLinkedinId());
        if (persistedUser != null)
            user.completeDataFromUser(persistedUser);
        LinkedinUser userFromApi = linkedinApi.getUserData(user.getLinkedinId());
        if (userFromApi != null)
            user.updateDataFromUser(userFromApi);
    }

}