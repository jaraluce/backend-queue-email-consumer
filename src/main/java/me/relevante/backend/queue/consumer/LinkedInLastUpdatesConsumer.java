package me.relevante.backend.queue.consumer;

import me.relevante.backend.queue.QueueNames;
import me.relevante.backend.queue.persistence.LinkedinPostMongoDao;
import me.relevante.backend.queue.persistence.LinkedinUserRepo;
import me.relevante.backend.queue.persistence.RelatedUserRepo;
import me.relevante.backend.queue.requests.ProcessContactsRequest;
import me.relevante.linkedin.api.LinkedinApi;
import me.relevante.linkedin.model.LinkedinPost;
import me.relevante.linkedin.model.LinkedinUser;
import me.relevante.model.Network;
import me.relevante.model.RelatedUser;
import me.relevante.model.oauth.OAuthKeyPair;
import me.relevante.model.oauth.OAuthTokenPair;
import me.relevante.nlp.Classifier;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Component
public class LinkedInLastUpdatesConsumer extends BaseConsumer {

    @Autowired
    private LinkedinUserRepo linkedinUserRepo;
    @Autowired
    private RelatedUserRepo relatedUserRepo;
    @Autowired
    private LinkedinPostMongoDao linkedinPostDao;
    @Autowired
    private Classifier classifier;
    @Autowired
    private OAuthKeyPair oAuthConsumerKeyPair;


	@RabbitListener(queues = QueueNames.LINKEDIN_SHARES)
	public void processContactsLinkedin(byte[] data) throws IOException {

		ProcessContactsRequest processContactsRequest = objectMapper.readValue(data, ProcessContactsRequest.class);
        OAuthTokenPair oAuthTokenPair = new OAuthTokenPair(processContactsRequest.getToken(), processContactsRequest.getSecret());
        LinkedinApi linkedinApi = new LinkedinApi(oAuthConsumerKeyPair, oAuthTokenPair);

        List<LinkedinPost> lastUpdates = linkedinApi.getUserTimeline();
        processUpdates(lastUpdates, linkedinApi, processContactsRequest.getIdRelevante());

	}

    private void processUpdates(List<LinkedinPost> updates,
                                LinkedinApi linkedinApi,
                                Integer relevanteId) throws IOException {

        List<LinkedinUser> linkedinUsers = getAllUsersFromShares(updates);
        for (LinkedinUser linkedinUser : linkedinUsers) {
            populateUser(linkedinUser, linkedinApi);
        }

        for (LinkedinPost update : updates) {
            if (update.getAuthor() != null && update.getAuthor().getUrl() != null) {
                LinkedinPost persistedPost = linkedinPostDao.findByPostId(update.getPostId());
                if (persistedPost != null) {
                    update.setId(persistedPost.getId());
                    update.setStems(new LinkedList<>(persistedPost.getStems()));
                    update.setKeywords(new LinkedList<>(persistedPost.getKeywords()));
                }
                else {
                    classifier.assignKeywords(update);
                }
                linkedinPostDao.save(update);
            }
        }

        for (LinkedinUser linkedinUser : linkedinUsers) {
            if (linkedinUser.getUrl() != null) {
                classifier.assignKeywords(linkedinUser);
                linkedinUserRepo.save(linkedinUser);
                relatedUserRepo.save(new RelatedUser(relevanteId, linkedinUser.getLinkedinId(), Network.LINKEDIN, true));
            }
        }

    }

    private List<LinkedinUser> getAllUsersFromShares(List<LinkedinPost> linkedinGroupShares) {

        // Create a map containing unique users as much updated as possible
        Map<String, LinkedinUser> usersMap = new HashMap<>();
        for (LinkedinPost linkedinGroupShare : linkedinGroupShares) {
            LinkedinUser userInMap = usersMap.get(linkedinGroupShare.getUserId());
            if (userInMap == null) {
                usersMap.put(linkedinGroupShare.getAuthor().getLinkedinId(), linkedinGroupShare.getAuthor());
            }
            else {
                userInMap.updateDataFromUser(linkedinGroupShare.getAuthor());
            }
        }

        // Put the obtained users into a list to be returned
        List<LinkedinUser> linkedinUsers = new ArrayList<>(usersMap.values());

        return linkedinUsers;
    }

    private void populateUser(LinkedinUser user,
                              LinkedinApi linkedinApi) {
        LinkedinUser persistedUser = linkedinUserRepo.findOne(user.getLinkedinId());
        if (persistedUser != null)
            user.completeDataFromUser(persistedUser);
        LinkedinUser userFromApi = linkedinApi.getUserData(user.getLinkedinId());
        if (userFromApi != null)
            user.updateDataFromUser(userFromApi);
    }


}